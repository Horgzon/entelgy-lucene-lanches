# Frontend #

Primeiramente, clone o repositório do projeto via Git e acesse o diretório de frontend:

# SSH #
```
git clone git@bitbucket.org:Horgzon/entelgy-lucene-lanches.git && cd frontend
```
# HTTPS #
```
git clone https://Horgzon@bitbucket.org/Horgzon/entelgy-lucene-lanches.git && cd frontend
```

# Pré-Requisitos #

# Node.js: #
```
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
```
# Angular CLI: #

```
sudo npm install -g @angular/cli
```

# Dependências #

As bibliotecas dependentes são instaladas com o comando abaixo: 

```
npm install
```

O comando irá gerar a pasta node_modules, que armazenará todas as dependências do projeto.

# Execução #

```
ng serve -o
```