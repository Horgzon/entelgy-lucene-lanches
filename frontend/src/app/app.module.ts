import { PedidoModule } from './pedido/pedido.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { SolicitarLancheComponent } from './solicitar-lanche/solicitar-lanche.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app.routing.module';
import { AuthService } from './login/auth.service';
import { LoginModule } from './login/login.module';




@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    SolicitarLancheComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    PedidoModule,
    HttpModule
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
