import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FinalizarPedidoComponent } from './finalizar-pedido/finalizar-pedido.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [FinalizarPedidoComponent]
})
export class PedidoModule { }
