import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarLancheComponent } from './solicitar-lanche.component';

describe('SolicitarLancheComponent', () => {
  let component: SolicitarLancheComponent;
  let fixture: ComponentFixture<SolicitarLancheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarLancheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarLancheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
