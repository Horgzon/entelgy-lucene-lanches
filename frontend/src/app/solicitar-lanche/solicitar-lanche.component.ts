import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-solicitar-lanche',
  templateUrl: './solicitar-lanche.component.html',
  styleUrls: ['./solicitar-lanche.component.css']
})
export class SolicitarLancheComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  finalizarPedido(){
    this.router.navigate(['finalizarPedido']);
  }

}
