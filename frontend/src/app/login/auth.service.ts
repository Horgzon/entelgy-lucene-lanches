import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Usuario } from './usuario';


@Injectable()
export class AuthService {

  private urlBase = 'http://localhost:8080/usuario';

  private headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost:8080'});

  private usuarioLogado: boolean = false;

  mostrarMenuEmitter = new EventEmitter<Boolean>();

  constructor(private router: Router, private http: Http) { }

  fazerLogin(usuario: Usuario) {
    if( usuario.usuario === 'teste'
      && usuario.senha === 'teste'){

      this.usuarioLogado = true;
      this.mostrarMenuEmitter.emit(true);
      this.router.navigate(['/']);

    } else {

      this.usuarioLogado = false;
      this.mostrarMenuEmitter.emit(false);

    }
  }

  cadastrarUsuario(usuario: Usuario): Promise<Usuario> {
     let usuarioAdicionado: Promise<Usuario>  =  this.http
      .post(this.urlBase, JSON.stringify(Usuario), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Usuario)
      .catch(this.handleError);

    this.usuarioLogado = true;
    this.mostrarMenuEmitter.emit(true);
    this.router.navigate(['/']);

    return usuarioAdicionado;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  usuarioEstaAutenticado(){
    return this.usuarioLogado;
  }

}
