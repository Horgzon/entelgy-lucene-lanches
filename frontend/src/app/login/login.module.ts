import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { LoginRoutingModule } from './login.routing.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LoginRoutingModule,
    FormsModule
  ],
  declarations: [
    LoginComponent, 
    CadastroComponent
  ]
})
export class LoginModule { }
