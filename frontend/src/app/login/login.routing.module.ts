import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from './login/login.component';
import { CadastroComponent } from './cadastro/cadastro.component';


const loginRoutes: Routes = [

    { path: 'login', component: LoginComponent },
    { path: 'login/cadastrar', component: CadastroComponent }

];

@NgModule({
    imports: [ RouterModule.forChild(loginRoutes) ],
    exports: [ RouterModule ]
})
export class LoginRoutingModule {}