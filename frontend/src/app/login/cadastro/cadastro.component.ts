import { Component, OnInit } from '@angular/core';

import { Usuario } from './../usuario';
import { AuthService } from './../auth.service';


@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  private usuario: Usuario = new Usuario();

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  cadastrar(){
    this.authService.cadastrarUsuario(this.usuario);
  }

}
