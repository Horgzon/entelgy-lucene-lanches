import { FinalizarPedidoComponent } from './pedido/finalizar-pedido/finalizar-pedido.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'

import { HomeComponent } from './home/home.component';
import { SolicitarLancheComponent } from './solicitar-lanche/solicitar-lanche.component';

const appRoutes: Routes = [

    { path: '', component: HomeComponent },
    { path: 'solicitarLanche', component: SolicitarLancheComponent },
    { path: 'finalizarPedido', component: FinalizarPedidoComponent }

];

@NgModule({
    imports: [ RouterModule.forRoot(appRoutes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}