package br.com.lucenelanches.pedidos.rest;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucenelanches.pedidos.model.Usuario;

@RestController
@RepositoryRestController
public class UsuarioRestController {

	@Autowired
	private UsuarioRestResource usuarioRestResource;
	

	@RequestMapping(method = RequestMethod.DELETE, value = "/usuario/{id}")
	public ResponseEntity<Usuario> excluirUsuario(@PathVariable Integer id) {
		ObjectId objectId = new ObjectId(id.toString());
		Usuario usuarioExcluir = usuarioRestResource.findOne(objectId);
		if (usuarioExcluir == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		usuarioRestResource.delete(usuarioExcluir);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/usuario", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> buscarUsuario(@RequestBody Usuario usuario) {
		List<Usuario> listaUsuario = usuarioRestResource.findAll();
		int posicao = listaUsuario.lastIndexOf(usuario);
		return new ResponseEntity<>(listaUsuario.get(posicao), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/usuario", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Usuario>> buscarTodosUsuarios() {
		List<Usuario> listaUsuario = usuarioRestResource.findAll();
		return new ResponseEntity<>(listaUsuario, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/usuario", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> cadastrarUsuario(@RequestBody Usuario usuario) {
		Usuario usuarioInserido = usuarioRestResource.save(usuario);
		return new ResponseEntity<>(usuarioInserido, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/usuario", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> atualizarUsuario(@RequestBody Usuario usuario) {
		Usuario usuarioAtualizado = usuarioRestResource.insert(usuario);
		return new ResponseEntity<>(usuarioAtualizado, HttpStatus.CREATED);
	}

}
