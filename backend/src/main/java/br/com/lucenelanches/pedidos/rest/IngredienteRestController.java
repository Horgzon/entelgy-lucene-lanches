package br.com.lucenelanches.pedidos.rest;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucenelanches.pedidos.model.Ingredientes;

@RestController
@RepositoryRestController
public class IngredienteRestController {
	
	@Autowired
	private IngredienteRestResource ingredienteRepository;
	
	@RequestMapping(method = RequestMethod.GET, value = "/ingredientes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Ingredientes> buscarIngredientes(){
		List<Ingredientes> listaIngredientes = ingredienteRepository.findAll();
		if (listaIngredientes.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		Ingredientes ingredientes = listaIngredientes.get(0);
		return new ResponseEntity<>(ingredientes, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/ingredientes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Ingredientes> cadastrarIngredientes(@RequestBody Ingredientes ingredientes){
		Ingredientes ingrediente = ingredienteRepository.save(ingredientes);
		return new ResponseEntity<>(ingrediente, HttpStatus.CREATED);
	}

}
