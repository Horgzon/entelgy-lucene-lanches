package br.com.lucenelanches.pedidos.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "usuario")
public class Usuario {

	@Id
	private ObjectId id;
	private String usuario;
	private String senha;
	private String nome;
	private Integer nivelAcesso;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(Integer nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	@Override
	public boolean equals(Object obj) {
		boolean igual = false;

		if (obj.getClass() == this.getClass()) {
			Usuario usuario = (Usuario) obj;

			if (getUsuario().equals(usuario.getUsuario()) && getSenha().equals(usuario.getSenha())) {
				igual = true;
			}

		}

		return igual;
	}

}
