package br.com.lucenelanches.pedidos.model;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ingredientes")
public class Ingredientes {
	
	@Id
	private ObjectId id;
	private List<Pao> paes = new ArrayList<Pao>();
	private List<Queijo> queijos = new ArrayList<Queijo>();
	private List<Recheio> recheios = new ArrayList<Recheio>();
	private List<Salada> saladas = new ArrayList<Salada>();
	private List<Molho> molhos = new ArrayList<Molho>();
	private List<Tempero> temperos = new ArrayList<Tempero>();
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public List<Pao> getPaes() {
		return paes;
	}
	public void setPaes(List<Pao> paes) {
		this.paes = paes;
	}
	public List<Queijo> getQueijos() {
		return queijos;
	}
	public void setQueijos(List<Queijo> queijos) {
		this.queijos = queijos;
	}
	public List<Recheio> getRecheios() {
		return recheios;
	}
	public void setRecheios(List<Recheio> recheios) {
		this.recheios = recheios;
	}
	public List<Salada> getSaladas() {
		return saladas;
	}
	public void setSaladas(List<Salada> saladas) {
		this.saladas = saladas;
	}
	public List<Molho> getMolhos() {
		return molhos;
	}
	public void setMolhos(List<Molho> molhos) {
		this.molhos = molhos;
	}
	public List<Tempero> getTemperos() {
		return temperos;
	}
	public void setTemperos(List<Tempero> temperos) {
		this.temperos = temperos;
	}
	
}
