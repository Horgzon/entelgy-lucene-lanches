package br.com.lucenelanches.pedidos.model;

public class Recheio {
	
	private String tipo;
	private String type;
	private Double valor;
	
	public Recheio() {
		super();
	}
	
	public Recheio(String tipo, String type, Double valor) {
		super();
		setTipo(tipo);
		setType(type);
		setValor(valor);
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}

}
