package br.com.lucenelanches.pedidos.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "pedido")
public class Pedido {
	
	@Id
	private ObjectId id;
	private String nome;
	private String end;
	private List<Lanche> listaLanches;
	private Double total;
	private Integer formaPagamento;
	private Boolean precisaTroco;
	private Integer qtdPessoas;
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public List<Lanche> getListaLanches() {
		return listaLanches;
	}
	public void setListaLanches(List<Lanche> listaLanches) {
		this.listaLanches = listaLanches;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Integer getFormaPagamento() {
		return formaPagamento;
	}
	public void setFormaPagamento(Integer formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public Boolean getPrecisaTroco() {
		return precisaTroco;
	}
	public void setPrecisaTroco(Boolean precisaTroco) {
		this.precisaTroco = precisaTroco;
	}
	public Integer getQtdPessoas() {
		return qtdPessoas;
	}
	public void setQtdPessoas(Integer qtdPessoas) {
		this.qtdPessoas = qtdPessoas;
	}
	

}
