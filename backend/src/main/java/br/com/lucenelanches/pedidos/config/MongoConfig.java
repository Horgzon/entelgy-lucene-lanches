package br.com.lucenelanches.pedidos.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClientURI;

@Configuration
public class MongoConfig {

	private static final String MONGODB_URI = "mongodb://entelgy:lucene@lucene-shard-00-00-gkk7o.mongodb.net:27017,lucene-shard-00-01-gkk7o.mongodb.net:27017,lucene-shard-00-02-gkk7o.mongodb.net:27017/test?ssl=true&replicaSet=Lucene-shard-0&authSource=admin";

	@Bean
	public MongoDbFactory mongoDbFactory() throws Exception {
		return new SimpleMongoDbFactory(new MongoClientURI(MONGODB_URI));
	}

	@Bean
	public MongoTemplate mongoTemplate() throws Exception {
		return new MongoTemplate(mongoDbFactory());
	}
}
