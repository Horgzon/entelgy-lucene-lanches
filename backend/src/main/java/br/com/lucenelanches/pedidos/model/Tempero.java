package br.com.lucenelanches.pedidos.model;

public class Tempero {
	
	private String tipo;
	private String type;
	
	public Tempero() {
		super();
	}
	
	public Tempero(String tipo, String type) {
		super();
		setTipo(tipo);
		setType(type);
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
