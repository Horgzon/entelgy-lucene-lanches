package br.com.lucenelanches.pedidos.rest;


import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucenelanches.pedidos.model.Pedido;

@RestController
@RepositoryRestController
public class PedidoRestController {
	
	@Autowired
	private PedidoRestResource pedidoRepository;
	

	@RequestMapping(method = RequestMethod.DELETE, value = "/pedido/{id}")
	public ResponseEntity<Pedido> excluirPedido(@PathVariable Integer id) {
		ObjectId objectId = new ObjectId(id.toString());
		Pedido pedidoExcluir = pedidoRepository.findOne(objectId);
		if (pedidoExcluir == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		pedidoRepository.delete(pedidoExcluir);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/pedido/{id}")
	public ResponseEntity<Pedido> buscarPedido(@PathVariable Integer id) {
		ObjectId objectId = new ObjectId(id.toString());
		Pedido pedidoExcluir = pedidoRepository.findOne(objectId);
		return new ResponseEntity<>(pedidoExcluir, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/pedido", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pedido>> buscarTodosPedido() {
		List<Pedido> listaPedido = pedidoRepository.findAll();
		return new ResponseEntity<>(listaPedido, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/pedido", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pedido> cadastrarPedido(@RequestBody Pedido pedido) {
		Pedido pedidoInserido = pedidoRepository.save(pedido);
		return new ResponseEntity<>(pedidoInserido, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/pedido", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pedido> atualizarPedido(@RequestBody Pedido pedido) {
		Pedido pedidoAtualizdo = pedidoRepository.insert(pedido);
		return new ResponseEntity<>(pedidoAtualizdo, HttpStatus.CREATED);
	}

	
}
