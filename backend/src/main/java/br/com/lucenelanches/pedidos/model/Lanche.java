package br.com.lucenelanches.pedidos.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "lanche")
public class Lanche {
	
	@Id
	private ObjectId id;
	private Pao pao;
	private Queijo queijo;
	private Boolean dobrarQueijo;
	
	private Recheio recheio;
	private Boolean dobrarRecheio;
	
	private Salada salada;
	private Boolean dobrarSalada;
	
	private List<Molho> molhos;
	private List<Tempero> temperos;
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public Pao getPao() {
		return pao;
	}
	public void setPao(Pao pao) {
		this.pao = pao;
	}
	public Queijo getQueijo() {
		return queijo;
	}
	public void setQueijo(Queijo queijo) {
		this.queijo = queijo;
	}
	public Boolean getDobrarQueijo() {
		return dobrarQueijo;
	}
	public void setDobrarQueijo(Boolean dobrarQueijo) {
		this.dobrarQueijo = dobrarQueijo;
	}
	public Recheio getRecheio() {
		return recheio;
	}
	public void setRecheio(Recheio recheio) {
		this.recheio = recheio;
	}
	public Boolean getDobrarRecheio() {
		return dobrarRecheio;
	}
	public void setDobrarRecheio(Boolean dobrarRecheio) {
		this.dobrarRecheio = dobrarRecheio;
	}
	public Salada getSalada() {
		return salada;
	}
	public void setSalada(Salada salada) {
		this.salada = salada;
	}
	public Boolean getDobrarSalada() {
		return dobrarSalada;
	}
	public void setDobrarSalada(Boolean dobrarSalada) {
		this.dobrarSalada = dobrarSalada;
	}
	public List<Molho> getMolhos() {
		return molhos;
	}
	public void setMolhos(List<Molho> molhos) {
		this.molhos = molhos;
	}
	public List<Tempero> getTemperos() {
		return temperos;
	}
	public void setTemperos(List<Tempero> temperos) {
		this.temperos = temperos;
	}

}
