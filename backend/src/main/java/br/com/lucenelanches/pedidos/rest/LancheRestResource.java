package br.com.lucenelanches.pedidos.rest;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.lucenelanches.pedidos.model.Lanche;

@RepositoryRestResource(collectionResourceRel = "lanche", path = "lanche")
public interface LancheRestResource extends MongoRepository<Lanche, ObjectId> {

}
