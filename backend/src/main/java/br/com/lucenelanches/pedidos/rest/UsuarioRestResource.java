package br.com.lucenelanches.pedidos.rest;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.lucenelanches.pedidos.model.Usuario;

@RepositoryRestResource(collectionResourceRel = "usuario", path = "usuario")
public interface UsuarioRestResource extends MongoRepository<Usuario, ObjectId> {
	

}
