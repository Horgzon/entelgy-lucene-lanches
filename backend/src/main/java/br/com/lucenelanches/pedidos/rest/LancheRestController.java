package br.com.lucenelanches.pedidos.rest;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucenelanches.pedidos.model.Lanche;

@RestController
@RepositoryRestController
public class LancheRestController {
	
	@Autowired
	private LancheRestResource lancheRestResource;
	
	@RequestMapping(method = RequestMethod.GET, value = "/lanches", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Lanche>> buscarTodosLanches() {
		List<Lanche> listaLanches = lancheRestResource.findAll();
		return new ResponseEntity<>(listaLanches, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/lanches", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Lanche> cadastrarLanche(@RequestBody Lanche lanche) {
		Lanche lancheInserido = lancheRestResource.save(lanche);
		return new ResponseEntity<>(lancheInserido, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/lanches", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Lanche> atualizarLanche(@RequestBody Lanche lanche) {
		Lanche lancheAtualizado = lancheRestResource.insert(lanche);
		return new ResponseEntity<>(lancheAtualizado, HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/lanches/{id}")
	public ResponseEntity<Lanche> excluirLanche(@PathVariable Integer id) {
		ObjectId objectId = new ObjectId(id.toString());
		Lanche lancheExcluir = lancheRestResource.findOne(objectId);
		if (lancheExcluir == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		lancheRestResource.delete(lancheExcluir);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/lanches/{id}")
	public ResponseEntity<Lanche> buscarLanche(@PathVariable Integer id) {
		ObjectId objectId = new ObjectId(id.toString());
		Lanche lanche = lancheRestResource.findOne(objectId);
		return new ResponseEntity<>(lanche, HttpStatus.OK);
	}

}
