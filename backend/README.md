# Backend #

Primeiramente, clone o repositório do projeto via Git e acesse o diretório de backend:

# SSH #
```
git clone git@bitbucket.org:Horgzon/entelgy-lucene-lanches.git && cd frontend
```
# HTTPS #
```
git clone https://Horgzon@bitbucket.org/Horgzon/entelgy-lucene-lanches.git && cd frontend
```

# Pré-Requisitos #

# Java 8+ #

# Gradle: #

```
curl -s https://get.sdkman.io | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install gradle 3.0
```

# Execução #

Com os comandos acima executados com sucesso, executo o comando abaixo para instalar as bibliotecas dependentes, compilar e executar a aplicação.

```
gradle bootRun
```

OBS.: Em alguns sistemas ao utilizar o comando acima no terminal a inicialização pode parecer que não foi concluido ficando em 80%, mas isso não impacta o funcionamento do sistemas e pode ser utilizado normalmente.