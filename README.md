# Entelgy - Lucene Lanches #

Projeto construído de acordo com as especificações do "User Story – Solicitação de lanches".
Abaixo segue informações das tecnologias utilizadas e porque dessas escolhas.

O projeto foi dividido em backend e frontend, dentro da pasta de cada um existe o passo a passo para instalar e executar a aplicação.

# BackEnd #

* Java;
* Spring Boot: Permite a criação de aplicações Web em Java abstraindo configurações e o uso de XML.
* Spring Data REST: Analisa o modelo de domínio da aplicação e expõe seus recursos seguindo a semântica do protocolo HTTP através do estilo arquitetural REST.
* Spring Data MongoDB: Fornece uma interface para integração com o banco de dados NoSQL MongoDB;
* Gradle: Sistema avançado de automatização de builds que une a flexibilidade do Ant com o gerenciamento de dependências e convenções do Maven.

# FrontEnd #

* HTML, CSS e JavaScript.
* Angular: Framework JavaScript com o conceito de SPA (Single Page Application) que prove poderosos recursos para a criação de webApp.
* Materializecss: É um framework CSS responsivo baseado no Material Design do Google.
* NPM: Node Package Manager, um flexível gerenciador de pacotes, builds e dependências feito em Node.js.
* Angular CLI: Automitiza a criação dos componentes do Angular.

# Comentários #

# Spring Boot, Data e REST #

Apesar de nunca ter utilizado esses frameworks, o poder e a facilidade de uso fizeram com que eles fossem escolhidos, a curva de aprendizado foi muito menor do que o esperado.

# NoSQL - MongoDB #

O banco de dados escolhido foi um NoSQL pois apresenta uma melhor performance para grandes cargas de trabalho e uma arquitetura de dados mais simplificada.  

# Angular e Materializecss #

Mesmo nunca tendo utilizado o Angular e o Materializecss, seu poder e funcionalidades que disponibilizam foram fundamentais para a sua escolhe, mesmo tendo uma curva de aprendizado um pouco maior do que o esperado.

# TODO #

Implementações futuras

* Melhoria no sistema de login e segurança.
* Desenvolver a especificação "User Story – Gerenciar entrega de pedidos".
* Internacionalização: criar a internacionalização no front e banckend através de uma aréa comum.
* Melhoria na arquitetura do forntend.
* Correção de problema de JavaScript na pagina de fazer pedido que exige o reload da página para o correto funcionamento.
